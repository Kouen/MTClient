#!/usr/bin/env python
# -*- coding: utf-8 -*-



import ot_pb2
import base_pb2
import stock_trade_pb2
from twisted.internet import reactor
from twisted.internet import task
import random

def pack(msg, send, type):
    body = send.SerializeToString()
    head = list(msg['head'])
    head[2] = type
    head[0] = len(body)
    return head, body

def CMD_LOGIN_REQ(message, protocol):
    recv = base_pb2.LoginReq()
    recv.ParseFromString(message['body'])
    send = base_pb2.LoginResp()
    send.result = 0
    return pack(message, send, base_pb2.CMD_LOGIN_RESP), 'CMD_LOGIN_REQ'

def CMD_SINGLE_ORDER_REQ(message, protocol):
    recv = stock_trade_pb2.SingleOrderReq()
    recv.ParseFromString(message['body'])
    send = stock_trade_pb2.SingleOrderResp()
    send.ret_code = 0
    send.order_no = '1'
    return pack(message, send, base_pb2.CMD_SINGLE_ORDER_RESP), 'CMD_SINGLE_ORDER_REQ'

def CMD_SINGLE_WITHDRAWAL_REQ(message, protocol):
    recv = stock_trade_pb2.SingleCancelReq()
    recv.ParseFromString(message['body'])
    send = stock_trade_pb2.SingleOrderResp()
    send.ret_code = 0
    send.order_no = '2'
    return pack(message, send, base_pb2.CMD_SINGLE_WITHDRAWAL_RESP), 'CMD_SINGLE_WITHDRAWAL_REQ'

def CMD_BASKET_ORDER_REQ(message, protocol):
    recv = ot_pb2.BasketPolicyReq()
    recv.ParseFromString(message['body'])
    send = ot_pb2.BasketPolicyResp()
    send.ret_code = 0
    send.policy_id = '1'
    send.rate = 0.0
    send.ksh_count = 0
    send.yg_cost = 10000
    send.finished_asset = 0
    send.finished_iopv = 0
    return pack(message, send, base_pb2.CMD_BASKET_ORDER_RESP), 'CMD_BASKET_ORDER_REQ'

def CMD_BASKET_WITHDRAWAL_REQ(message, protocol):
    recv = ot_pb2.BasketWithdrawalReq()
    recv.ParseFromString(message['body'])
    send = ot_pb2.BasketWithdrawalResp()
    send.ret_code = 0
    send.policy_id = '1'
    return pack(message, send, base_pb2.CMD_BASKET_WITHDRAWAL_RESP), 'CMD_BASKET_WITHDRAWAL_REQ'

def CMD_BASKET_CUSTOM_REQ(message, protocol):
    recv = ot_pb2.BasketCustomReq()
    recv.ParseFromString(message['body'])
    send = ot_pb2.BasketPolicyResp()
    send.ret_code = 0
    send.policy_id = '1'
    send.rate = 0.0
    send.ksh_count = 0
    send.yg_cost = 10000
    send.finished_asset = 0
    send.finished_iopv = 0
    return pack(message, send, base_pb2.CMD_BASKET_CUSTOM_RESP), 'CMD_BASKET_CUSTOM_REQ'

def CMD_UNFINISH_BASKET_REQ(message, protocol):
    send = ot_pb2.BasketUnfinishedResp()
    send.ret_code = 0
    son = send.baskets.add()
    son.code = '510050'
    son.volume = 2
    son.opr = base_pb2.OPR_BUY
    son.money = 99
    son.rate = 1.5
    son.policyid = 1

    son = send.baskets.add()
    son.code = '510030'
    son.volume = 1
    son.opr = base_pb2.OPR_BUY
    son.money = 2000
    son.rate = 10
    son.policyid = 2
    return pack(message, send, base_pb2.CMD_UNFINISH_BASKET_RESP), 'CMD_UNFINISH_BASKET_REQ'

def CMD_GRADED_FUND_REQ(message, protocol):
    recv = ot_pb2.GradingFundReq()
    recv.ParseFromString(message['body'])
    send = ot_pb2.PolicyResp()
    send.ret_code = 0
    send.policy_id = '1'
    return pack(message, send, base_pb2.CMD_GRADED_FUND_RESP), 'CMD_GRADED_FUND_REQ'

import SocketTools
gReqID = 10000

def PUB_ETF_LIMIT_UP(protocol):
    global gReqID
    send = ot_pb2.EtfInfoUpdateReq()
    send.code = '510050'
    send.value = random.randint(1, 1000)
    body = send.SerializeToString()
    tools = SocketTools.StructProtocol()
    gReqID += 1
    protocol.transport.write(tools.pack([len(body), 0x9999, base_pb2.CMD_SUB_ETF_INFO_LIMIT_UP_REQ, str(gReqID)], body))
    reactor.callLater(1.0, PUB_ETF_LIMIT_UP, protocol)

def PUB_ETF_LIMIT_DOWN(protocol):
    global gReqID
    send = ot_pb2.EtfInfoUpdateReq()
    send.code = '510050'
    send.value = random.randint(1, 1000)
    body = send.SerializeToString()
    tools = SocketTools.StructProtocol()
    gReqID += 1
    protocol.transport.write(tools.pack([len(body), 0x9999, base_pb2.CMD_SUB_ETF_INFO_LIMIT_DOWN_REQ, str(gReqID)], body))
    reactor.callLater(1.0, PUB_ETF_LIMIT_DOWN, protocol)

def PUB_ETF_STOP(protocol):
    global gReqID
    send = ot_pb2.EtfInfoUpdateReq()
    send.code = '510050'
    send.value = random.randint(1, 1000)
    body = send.SerializeToString()
    tools = SocketTools.StructProtocol()
    gReqID += 1
    protocol.transport.write(tools.pack([len(body), 0x9999, base_pb2.CMD_SUB_ETF_INFO_STOP_REQ, str(gReqID)], body))
    reactor.callLater(1.0, PUB_ETF_STOP, protocol)

def PUB_ETF_MAXPRICE(protocol):
    global gReqID
    send = ot_pb2.EtfInfoUpdateReq()
    send.code = '510050'
    send.value = random.randint(1, 1000)
    body = send.SerializeToString()
    tools = SocketTools.StructProtocol()
    gReqID += 1
    protocol.transport.write(tools.pack([len(body), 0x9999, base_pb2.CMD_SUB_ETF_INFO_MAXPRICE_REQ, str(gReqID)], body))
    reactor.callLater(1.0, PUB_ETF_MAXPRICE, protocol)

def PUB_ETF_MINPRICE(protocol):
    global gReqID
    send = ot_pb2.EtfInfoUpdateReq()
    send.code = '510050'
    send.value = random.randint(1, 1000)
    body = send.SerializeToString()
    tools = SocketTools.StructProtocol()
    gReqID += 1
    protocol.transport.write(tools.pack([len(body), 0x9999, base_pb2.CMD_SUB_ETF_INFO_MINPRICE_REQ, str(gReqID)], body))
    reactor.callLater(1.0, PUB_ETF_MINPRICE, protocol)

def PUB_ETF_DIOPV_B1(protocol):
    global gReqID
    send = ot_pb2.EtfInfoUpdateReq()
    send.code = '510050'
    send.value = random.randint(1, 1000)
    body = send.SerializeToString()
    tools = SocketTools.StructProtocol()
    gReqID += 1
    protocol.transport.write(tools.pack([len(body), 0x9999, base_pb2.CMD_SUB_ETF_INFO_DIOPV_B1_REQ, str(gReqID)], body))
    reactor.callLater(1.0, PUB_ETF_DIOPV_B1, protocol)

def PUB_ETF_DIOPV_S1(protocol):
    global gReqID
    send = ot_pb2.EtfInfoUpdateReq()
    send.code = '510050'
    send.value = random.randint(1, 1000)
    body = send.SerializeToString()
    tools = SocketTools.StructProtocol()
    gReqID += 1
    protocol.transport.write(tools.pack([len(body), 0x9999, base_pb2.CMD_SUB_ETF_INFO_DIOPV_S1_REQ, str(gReqID)], body))
    reactor.callLater(1.0, PUB_ETF_DIOPV_S1, protocol)

def PUB_ETF_BASIS_B1(protocol):
    global gReqID
    send = ot_pb2.EtfInfoUpdateReq()
    send.code = '510050'
    send.value = random.randint(1, 1000)
    body = send.SerializeToString()
    tools = SocketTools.StructProtocol()
    gReqID += 1
    protocol.transport.write(tools.pack([len(body), 0x9999, base_pb2.CMD_SUB_ETF_INFO_BASIS_B1_REQ, str(gReqID)], body))
    reactor.callLater(1.0, PUB_ETF_BASIS_B1, protocol)

def PUB_ETF_BASIS_S1(protocol):
    global gReqID
    send = ot_pb2.EtfInfoUpdateReq()
    send.code = '510050'
    send.value = random.randint(1, 1000)
    body = send.SerializeToString()
    tools = SocketTools.StructProtocol()
    gReqID += 1
    protocol.transport.write(tools.pack([len(body), 0x9999, base_pb2.CMD_SUB_ETF_INFO_BASIS_S1_REQ, str(gReqID)], body))
    reactor.callLater(1.0, PUB_ETF_BASIS_S1, protocol)

def PUB_ETF_BASIS_OPEN(protocol):
    global gReqID
    send = ot_pb2.EtfInfoUpdateReq()
    send.code = '510050'
    send.value = random.randint(1, 1000)
    body = send.SerializeToString()
    tools = SocketTools.StructProtocol()
    gReqID += 1
    protocol.transport.write(tools.pack([len(body), 0x9999, base_pb2.CMD_SUB_ETF_INFO_BASIS_OPEN_REQ, str(gReqID)], body))
    reactor.callLater(1.0, PUB_ETF_BASIS_OPEN, protocol)

def PUB_ETF_BASIS_CLOSE(protocol):
    global gReqID
    send = ot_pb2.EtfInfoUpdateReq()
    send.code = '510050'
    send.value = random.randint(1, 1000)
    body = send.SerializeToString()
    tools = SocketTools.StructProtocol()
    gReqID += 1
    protocol.transport.write(tools.pack([len(body), 0x9999, base_pb2.CMD_SUB_ETF_INFO_BASIS_CLOSE_REQ, str(gReqID)], body))
    reactor.callLater(1.0, PUB_ETF_BASIS_CLOSE, protocol)

def CMD_SUB_ETF_INFO_REQ(message, protocol):
    recv = ot_pb2.EtfInfoSubReq()
    recv.ParseFromString(message['body'])

    if recv.sub == base_pb2.SUB_ETF_INFO:
        reactor.callLater(1.0, PUB_ETF_LIMIT_UP, protocol)
        reactor.callLater(1.0, PUB_ETF_LIMIT_DOWN, protocol)
        reactor.callLater(1.0, PUB_ETF_STOP, protocol)
        reactor.callLater(1.0, PUB_ETF_MAXPRICE, protocol)
        reactor.callLater(1.0, PUB_ETF_MINPRICE, protocol)
        reactor.callLater(1.0, PUB_ETF_DIOPV_B1, protocol)
        reactor.callLater(1.0, PUB_ETF_DIOPV_S1, protocol)
    elif recv.sub == base_pb2.SUB_ETF_BASIS:
        reactor.callLater(1.0, PUB_ETF_BASIS_OPEN, protocol)
        reactor.callLater(1.0, PUB_ETF_BASIS_CLOSE, protocol)
        reactor.callLater(1.0, PUB_ETF_BASIS_S1, protocol)
        reactor.callLater(1.0, PUB_ETF_BASIS_B1, protocol)
    elif recv.sub == base_pb2.SUB_CUSTOM_BASIS:
        reactor.callLater(1.0, PUB_ETF_BASIS_OPEN, protocol)
        reactor.callLater(1.0, PUB_ETF_BASIS_CLOSE, protocol)
        reactor.callLater(1.0, PUB_ETF_BASIS_S1, protocol)
        reactor.callLater(1.0, PUB_ETF_BASIS_B1, protocol)

    send = ot_pb2.EtfInfoSubResp()
    send.ret_code = 0
    send.code = '399330'
    return pack(message, send, base_pb2.CMD_SUB_ETF_INFO_RESP), 'CMD_SUB_ETF_INFO_REQ'

def CMD_SUB_CUSTOM_BASIS_REQ(message, protocol):
    recv = ot_pb2.CustomBasisSubReq()
    recv.ParseFromString(message['body'])
    send = ot_pb2.EtfInfoSubResp()
    send.ret_code = 0
    send.code = 'DIY10086'
    return pack(message, send, base_pb2.CMD_SUB_CUSTOM_BASIS_RESP), 'CMD_SUB_CUSTOM_BASIS_REQ'

def CMD_ETF_ORDER_REQ(message, protocol):
    recv = stock_trade_pb2.SingleOrderReq()
    recv.ParseFromString(message['body'])
    send = stock_trade_pb2.SingleOrderResp()
    send.ret_code = 0
    return pack(message, send, base_pb2.CMD_ETF_ORDER_RESP), 'CMD_ETF_ORDER_REQ'

def CMD_FUND_ORDER_REQ(message, protocol):
    recv = stock_trade_pb2.SingleOrderReq()
    recv.ParseFromString(message['body'])
    send = stock_trade_pb2.SingleOrderResp()
    send.ret_code = 0
    return pack(message, send, base_pb2.CMD_FUND_ORDER_RESP), 'CMD_FUND_ORDER_REQ'


SWITCH = {
    base_pb2.CMD_LOGIN_REQ: CMD_LOGIN_REQ,
    base_pb2.CMD_SINGLE_ORDER_REQ: CMD_SINGLE_ORDER_REQ,
    base_pb2.CMD_SINGLE_WITHDRAWAL_REQ: CMD_SINGLE_WITHDRAWAL_REQ,
    base_pb2.CMD_BASKET_ORDER_REQ: CMD_BASKET_ORDER_REQ,
    base_pb2.CMD_BASKET_WITHDRAWAL_REQ: CMD_BASKET_WITHDRAWAL_REQ,
    base_pb2.CMD_BASKET_CUSTOM_REQ: CMD_BASKET_CUSTOM_REQ,
    base_pb2.CMD_UNFINISH_BASKET_REQ: CMD_UNFINISH_BASKET_REQ,
    base_pb2.CMD_GRADED_FUND_REQ: CMD_GRADED_FUND_REQ,
    base_pb2.CMD_SUB_ETF_INFO_REQ: CMD_SUB_ETF_INFO_REQ,
    base_pb2.CMD_SUB_CUSTOM_BASIS_REQ: CMD_SUB_CUSTOM_BASIS_REQ,
    base_pb2.CMD_ETF_ORDER_REQ: CMD_ETF_ORDER_REQ,
    base_pb2.CMD_FUND_ORDER_REQ: CMD_FUND_ORDER_REQ,
}


def Handle(message, protocol):
    return SWITCH[message['head'][2]](message, protocol)

