# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: trade_risk_ctrl.proto

from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)




DESCRIPTOR = _descriptor.FileDescriptor(
  name='trade_risk_ctrl.proto',
  package='trade_risk_ctrl',
  serialized_pb='\n\x15trade_risk_ctrl.proto\x12\x0ftrade_risk_ctrl\".\n\x13StopAllowTradingReq\x12\x17\n\x0fstop_allow_type\x18\x01 \x02(\x05\"=\n\x14StopAllowTradingResp\x12\x10\n\x08ret_code\x18\x01 \x02(\x05\x12\x13\n\x0bret_message\x18\x02 \x01(\t\"\'\n\x10\x43\x61ncelTradingReq\x12\x13\n\x0b\x63\x61ncel_type\x18\x01 \x02(\x05\":\n\x11\x43\x61ncelTradingResp\x12\x10\n\x08ret_code\x18\x01 \x02(\x05\x12\x13\n\x0bret_message\x18\x02 \x01(\t\"g\n\x15QuerySystemStatusResp\x12\x10\n\x08ret_code\x18\x01 \x02(\x05\x12\x13\n\x0bret_message\x18\x02 \x01(\t\x12\x12\n\nbuy_status\x18\x03 \x01(\x05\x12\x13\n\x0bsell_status\x18\x04 \x01(\x05\")\n\x14QueryTraderStatusReq\x12\x11\n\ttrader_id\x18\x01 \x01(\t\"\xd4\x01\n\x15QueryTraderStatusResp\x12\x10\n\x08ret_code\x18\x01 \x02(\x05\x12\x13\n\x0bret_message\x18\x02 \x01(\t\x12H\n\x0btrader_list\x18\x03 \x03(\x0b\x32\x33.trade_risk_ctrl.QueryTraderStatusResp.TraderStatus\x1aJ\n\x0cTraderStatus\x12\x11\n\ttrader_id\x18\x01 \x02(\t\x12\x12\n\nbuy_status\x18\x02 \x01(\x05\x12\x13\n\x0bsell_status\x18\x03 \x01(\x05\"@\n\x12SetTraderStatusReq\x12\x11\n\ttrader_id\x18\x01 \x02(\t\x12\x17\n\x0fstop_allow_type\x18\x02 \x02(\x05\"<\n\x13SetTraderStatusResp\x12\x10\n\x08ret_code\x18\x01 \x02(\x05\x12\x13\n\x0bret_message\x18\x02 \x01(\t')




_STOPALLOWTRADINGREQ = _descriptor.Descriptor(
  name='StopAllowTradingReq',
  full_name='trade_risk_ctrl.StopAllowTradingReq',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='stop_allow_type', full_name='trade_risk_ctrl.StopAllowTradingReq.stop_allow_type', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=42,
  serialized_end=88,
)


_STOPALLOWTRADINGRESP = _descriptor.Descriptor(
  name='StopAllowTradingResp',
  full_name='trade_risk_ctrl.StopAllowTradingResp',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ret_code', full_name='trade_risk_ctrl.StopAllowTradingResp.ret_code', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='ret_message', full_name='trade_risk_ctrl.StopAllowTradingResp.ret_message', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=unicode("", "utf-8"),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=90,
  serialized_end=151,
)


_CANCELTRADINGREQ = _descriptor.Descriptor(
  name='CancelTradingReq',
  full_name='trade_risk_ctrl.CancelTradingReq',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='cancel_type', full_name='trade_risk_ctrl.CancelTradingReq.cancel_type', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=153,
  serialized_end=192,
)


_CANCELTRADINGRESP = _descriptor.Descriptor(
  name='CancelTradingResp',
  full_name='trade_risk_ctrl.CancelTradingResp',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ret_code', full_name='trade_risk_ctrl.CancelTradingResp.ret_code', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='ret_message', full_name='trade_risk_ctrl.CancelTradingResp.ret_message', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=unicode("", "utf-8"),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=194,
  serialized_end=252,
)


_QUERYSYSTEMSTATUSRESP = _descriptor.Descriptor(
  name='QuerySystemStatusResp',
  full_name='trade_risk_ctrl.QuerySystemStatusResp',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ret_code', full_name='trade_risk_ctrl.QuerySystemStatusResp.ret_code', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='ret_message', full_name='trade_risk_ctrl.QuerySystemStatusResp.ret_message', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=unicode("", "utf-8"),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='buy_status', full_name='trade_risk_ctrl.QuerySystemStatusResp.buy_status', index=2,
      number=3, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='sell_status', full_name='trade_risk_ctrl.QuerySystemStatusResp.sell_status', index=3,
      number=4, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=254,
  serialized_end=357,
)


_QUERYTRADERSTATUSREQ = _descriptor.Descriptor(
  name='QueryTraderStatusReq',
  full_name='trade_risk_ctrl.QueryTraderStatusReq',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='trader_id', full_name='trade_risk_ctrl.QueryTraderStatusReq.trader_id', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=unicode("", "utf-8"),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=359,
  serialized_end=400,
)


_QUERYTRADERSTATUSRESP_TRADERSTATUS = _descriptor.Descriptor(
  name='TraderStatus',
  full_name='trade_risk_ctrl.QueryTraderStatusResp.TraderStatus',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='trader_id', full_name='trade_risk_ctrl.QueryTraderStatusResp.TraderStatus.trader_id', index=0,
      number=1, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=unicode("", "utf-8"),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='buy_status', full_name='trade_risk_ctrl.QueryTraderStatusResp.TraderStatus.buy_status', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='sell_status', full_name='trade_risk_ctrl.QueryTraderStatusResp.TraderStatus.sell_status', index=2,
      number=3, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=541,
  serialized_end=615,
)

_QUERYTRADERSTATUSRESP = _descriptor.Descriptor(
  name='QueryTraderStatusResp',
  full_name='trade_risk_ctrl.QueryTraderStatusResp',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ret_code', full_name='trade_risk_ctrl.QueryTraderStatusResp.ret_code', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='ret_message', full_name='trade_risk_ctrl.QueryTraderStatusResp.ret_message', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=unicode("", "utf-8"),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='trader_list', full_name='trade_risk_ctrl.QueryTraderStatusResp.trader_list', index=2,
      number=3, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[_QUERYTRADERSTATUSRESP_TRADERSTATUS, ],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=403,
  serialized_end=615,
)


_SETTRADERSTATUSREQ = _descriptor.Descriptor(
  name='SetTraderStatusReq',
  full_name='trade_risk_ctrl.SetTraderStatusReq',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='trader_id', full_name='trade_risk_ctrl.SetTraderStatusReq.trader_id', index=0,
      number=1, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=unicode("", "utf-8"),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='stop_allow_type', full_name='trade_risk_ctrl.SetTraderStatusReq.stop_allow_type', index=1,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=617,
  serialized_end=681,
)


_SETTRADERSTATUSRESP = _descriptor.Descriptor(
  name='SetTraderStatusResp',
  full_name='trade_risk_ctrl.SetTraderStatusResp',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ret_code', full_name='trade_risk_ctrl.SetTraderStatusResp.ret_code', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='ret_message', full_name='trade_risk_ctrl.SetTraderStatusResp.ret_message', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=unicode("", "utf-8"),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=683,
  serialized_end=743,
)

_QUERYTRADERSTATUSRESP_TRADERSTATUS.containing_type = _QUERYTRADERSTATUSRESP;
_QUERYTRADERSTATUSRESP.fields_by_name['trader_list'].message_type = _QUERYTRADERSTATUSRESP_TRADERSTATUS
DESCRIPTOR.message_types_by_name['StopAllowTradingReq'] = _STOPALLOWTRADINGREQ
DESCRIPTOR.message_types_by_name['StopAllowTradingResp'] = _STOPALLOWTRADINGRESP
DESCRIPTOR.message_types_by_name['CancelTradingReq'] = _CANCELTRADINGREQ
DESCRIPTOR.message_types_by_name['CancelTradingResp'] = _CANCELTRADINGRESP
DESCRIPTOR.message_types_by_name['QuerySystemStatusResp'] = _QUERYSYSTEMSTATUSRESP
DESCRIPTOR.message_types_by_name['QueryTraderStatusReq'] = _QUERYTRADERSTATUSREQ
DESCRIPTOR.message_types_by_name['QueryTraderStatusResp'] = _QUERYTRADERSTATUSRESP
DESCRIPTOR.message_types_by_name['SetTraderStatusReq'] = _SETTRADERSTATUSREQ
DESCRIPTOR.message_types_by_name['SetTraderStatusResp'] = _SETTRADERSTATUSRESP

class StopAllowTradingReq(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _STOPALLOWTRADINGREQ

  # @@protoc_insertion_point(class_scope:trade_risk_ctrl.StopAllowTradingReq)

class StopAllowTradingResp(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _STOPALLOWTRADINGRESP

  # @@protoc_insertion_point(class_scope:trade_risk_ctrl.StopAllowTradingResp)

class CancelTradingReq(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _CANCELTRADINGREQ

  # @@protoc_insertion_point(class_scope:trade_risk_ctrl.CancelTradingReq)

class CancelTradingResp(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _CANCELTRADINGRESP

  # @@protoc_insertion_point(class_scope:trade_risk_ctrl.CancelTradingResp)

class QuerySystemStatusResp(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _QUERYSYSTEMSTATUSRESP

  # @@protoc_insertion_point(class_scope:trade_risk_ctrl.QuerySystemStatusResp)

class QueryTraderStatusReq(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _QUERYTRADERSTATUSREQ

  # @@protoc_insertion_point(class_scope:trade_risk_ctrl.QueryTraderStatusReq)

class QueryTraderStatusResp(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType

  class TraderStatus(_message.Message):
    __metaclass__ = _reflection.GeneratedProtocolMessageType
    DESCRIPTOR = _QUERYTRADERSTATUSRESP_TRADERSTATUS

    # @@protoc_insertion_point(class_scope:trade_risk_ctrl.QueryTraderStatusResp.TraderStatus)
  DESCRIPTOR = _QUERYTRADERSTATUSRESP

  # @@protoc_insertion_point(class_scope:trade_risk_ctrl.QueryTraderStatusResp)

class SetTraderStatusReq(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _SETTRADERSTATUSREQ

  # @@protoc_insertion_point(class_scope:trade_risk_ctrl.SetTraderStatusReq)

class SetTraderStatusResp(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _SETTRADERSTATUSRESP

  # @@protoc_insertion_point(class_scope:trade_risk_ctrl.SetTraderStatusResp)


# @@protoc_insertion_point(module_scope)
