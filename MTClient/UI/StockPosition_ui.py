# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'StockPosition.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_DockWidget(object):
    def setupUi(self, DockWidget):
        DockWidget.setObjectName(_fromUtf8("DockWidget"))
        DockWidget.resize(918, 401)
        self.dockWidgetContents = QtGui.QWidget()
        self.dockWidgetContents.setObjectName(_fromUtf8("dockWidgetContents"))
        self.tv_position_list = QtGui.QTableWidget(self.dockWidgetContents)
        self.tv_position_list.setGeometry(QtCore.QRect(0, 40, 911, 331))
        self.tv_position_list.setObjectName(_fromUtf8("tv_position_list"))
        self.tv_position_list.setColumnCount(9)
        self.tv_position_list.setRowCount(0)
        item = QtGui.QTableWidgetItem()
        self.tv_position_list.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.tv_position_list.setHorizontalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.tv_position_list.setHorizontalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        self.tv_position_list.setHorizontalHeaderItem(3, item)
        item = QtGui.QTableWidgetItem()
        self.tv_position_list.setHorizontalHeaderItem(4, item)
        item = QtGui.QTableWidgetItem()
        self.tv_position_list.setHorizontalHeaderItem(5, item)
        item = QtGui.QTableWidgetItem()
        self.tv_position_list.setHorizontalHeaderItem(6, item)
        item = QtGui.QTableWidgetItem()
        self.tv_position_list.setHorizontalHeaderItem(7, item)
        item = QtGui.QTableWidgetItem()
        self.tv_position_list.setHorizontalHeaderItem(8, item)
        self.rb_code = QtGui.QRadioButton(self.dockWidgetContents)
        self.rb_code.setGeometry(QtCore.QRect(240, 10, 100, 16))
        self.rb_code.setObjectName(_fromUtf8("rb_code"))
        self.le_code_data = QtGui.QLineEdit(self.dockWidgetContents)
        self.le_code_data.setGeometry(QtCore.QRect(350, 10, 201, 20))
        self.le_code_data.setObjectName(_fromUtf8("le_code_data"))
        self.cb_only_etf_fund = QtGui.QCheckBox(self.dockWidgetContents)
        self.cb_only_etf_fund.setGeometry(QtCore.QRect(20, 10, 111, 16))
        self.cb_only_etf_fund.setObjectName(_fromUtf8("cb_only_etf_fund"))
        self.cb_no_zero_position = QtGui.QCheckBox(self.dockWidgetContents)
        self.cb_no_zero_position.setGeometry(QtCore.QRect(140, 10, 91, 16))
        self.cb_no_zero_position.setObjectName(_fromUtf8("cb_no_zero_position"))
        self.pb_export = QtGui.QPushButton(self.dockWidgetContents)
        self.pb_export.setGeometry(QtCore.QRect(640, 10, 75, 23))
        self.pb_export.setObjectName(_fromUtf8("pb_export"))
        self.pb_search = QtGui.QPushButton(self.dockWidgetContents)
        self.pb_search.setGeometry(QtCore.QRect(560, 10, 75, 23))
        self.pb_search.setObjectName(_fromUtf8("pb_search"))
        DockWidget.setWidget(self.dockWidgetContents)

        self.retranslateUi(DockWidget)
        QtCore.QMetaObject.connectSlotsByName(DockWidget)

    def retranslateUi(self, DockWidget):
        DockWidget.setWindowTitle(_translate("DockWidget", "现货持仓", None))
        item = self.tv_position_list.horizontalHeaderItem(0)
        item.setText(_translate("DockWidget", "证券代码", None))
        item = self.tv_position_list.horizontalHeaderItem(1)
        item.setText(_translate("DockWidget", "证券名称", None))
        item = self.tv_position_list.horizontalHeaderItem(2)
        item.setText(_translate("DockWidget", "持仓数", None))
        item = self.tv_position_list.horizontalHeaderItem(3)
        item.setText(_translate("DockWidget", "持仓市值(万元)", None))
        item = self.tv_position_list.horizontalHeaderItem(4)
        item.setText(_translate("DockWidget", "可卖数", None))
        item = self.tv_position_list.horizontalHeaderItem(5)
        item.setText(_translate("DockWidget", "可卖市值(万元)", None))
        item = self.tv_position_list.horizontalHeaderItem(6)
        item.setText(_translate("DockWidget", "可赎回数", None))
        item = self.tv_position_list.horizontalHeaderItem(7)
        item.setText(_translate("DockWidget", "已申篮子数", None))
        item = self.tv_position_list.horizontalHeaderItem(8)
        item.setText(_translate("DockWidget", "已赎篮子数", None))
        self.rb_code.setText(_translate("DockWidget", "证券代码筛选", None))
        self.cb_only_etf_fund.setText(_translate("DockWidget", "只显示ETF和基金", None))
        self.cb_no_zero_position.setText(_translate("DockWidget", "不显示0持仓", None))
        self.pb_export.setText(_translate("DockWidget", "导出", None))
        self.pb_search.setText(_translate("DockWidget", "查询", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    DockWidget = QtGui.QDockWidget()
    ui = Ui_DockWidget()
    ui.setupUi(DockWidget)
    DockWidget.show()
    sys.exit(app.exec_())

