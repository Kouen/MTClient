# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'UnfinishedBasket.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(625, 268)
        self.tv_unfinished_basket_list = QtGui.QTableWidget(Dialog)
        self.tv_unfinished_basket_list.setGeometry(QtCore.QRect(10, 40, 601, 192))
        self.tv_unfinished_basket_list.setObjectName(_fromUtf8("tv_unfinished_basket_list"))
        self.tv_unfinished_basket_list.setColumnCount(7)
        self.tv_unfinished_basket_list.setRowCount(0)
        item = QtGui.QTableWidgetItem()
        self.tv_unfinished_basket_list.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.tv_unfinished_basket_list.setHorizontalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.tv_unfinished_basket_list.setHorizontalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        self.tv_unfinished_basket_list.setHorizontalHeaderItem(3, item)
        item = QtGui.QTableWidgetItem()
        self.tv_unfinished_basket_list.setHorizontalHeaderItem(4, item)
        item = QtGui.QTableWidgetItem()
        self.tv_unfinished_basket_list.setHorizontalHeaderItem(5, item)
        item = QtGui.QTableWidgetItem()
        self.tv_unfinished_basket_list.setHorizontalHeaderItem(6, item)
        self.tv_unfinished_basket_list.horizontalHeader().setDefaultSectionSize(70)
        self.tv_unfinished_basket_list.horizontalHeader().setStretchLastSection(True)
        self.label = QtGui.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(20, 10, 101, 16))
        self.label.setObjectName(_fromUtf8("label"))
        self.pb_choose = QtGui.QPushButton(Dialog)
        self.pb_choose.setGeometry(QtCore.QRect(70, 240, 75, 23))
        self.pb_choose.setObjectName(_fromUtf8("pb_choose"))
        self.pb_cancel = QtGui.QPushButton(Dialog)
        self.pb_cancel.setGeometry(QtCore.QRect(470, 240, 75, 23))
        self.pb_cancel.setObjectName(_fromUtf8("pb_cancel"))

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "未完成篮子", None))
        item = self.tv_unfinished_basket_list.horizontalHeaderItem(0)
        item.setText(_translate("Dialog", "选择", None))
        item = self.tv_unfinished_basket_list.horizontalHeaderItem(1)
        item.setText(_translate("Dialog", "篮子代码", None))
        item = self.tv_unfinished_basket_list.horizontalHeaderItem(2)
        item.setText(_translate("Dialog", "篮子数量", None))
        item = self.tv_unfinished_basket_list.horizontalHeaderItem(3)
        item.setText(_translate("Dialog", "买卖方向", None))
        item = self.tv_unfinished_basket_list.horizontalHeaderItem(4)
        item.setText(_translate("Dialog", "已成金额", None))
        item = self.tv_unfinished_basket_list.horizontalHeaderItem(5)
        item.setText(_translate("Dialog", "完成百分比", None))
        item = self.tv_unfinished_basket_list.horizontalHeaderItem(6)
        item.setText(_translate("Dialog", "策略ID", None))
        self.label.setText(_translate("Dialog", "选择未完成篮子：", None))
        self.pb_choose.setText(_translate("Dialog", "选择", None))
        self.pb_cancel.setText(_translate("Dialog", "取消", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog = QtGui.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

