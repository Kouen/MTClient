# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ReplaceComplement.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(469, 291)
        self.pb_plbz = QtGui.QPushButton(Dialog)
        self.pb_plbz.setGeometry(QtCore.QRect(70, 260, 75, 23))
        self.pb_plbz.setObjectName(_fromUtf8("pb_plbz"))
        self.pb_cancel = QtGui.QPushButton(Dialog)
        self.pb_cancel.setGeometry(QtCore.QRect(310, 260, 75, 23))
        self.pb_cancel.setObjectName(_fromUtf8("pb_cancel"))
        self.tv_data_list = QtGui.QTableWidget(Dialog)
        self.tv_data_list.setGeometry(QtCore.QRect(10, 50, 451, 201))
        self.tv_data_list.setObjectName(_fromUtf8("tv_data_list"))
        self.tv_data_list.setColumnCount(7)
        self.tv_data_list.setRowCount(0)
        item = QtGui.QTableWidgetItem()
        self.tv_data_list.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.tv_data_list.setHorizontalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.tv_data_list.setHorizontalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        self.tv_data_list.setHorizontalHeaderItem(3, item)
        item = QtGui.QTableWidgetItem()
        self.tv_data_list.setHorizontalHeaderItem(4, item)
        item = QtGui.QTableWidgetItem()
        self.tv_data_list.setHorizontalHeaderItem(5, item)
        item = QtGui.QTableWidgetItem()
        self.tv_data_list.setHorizontalHeaderItem(6, item)
        self.tv_data_list.horizontalHeader().setDefaultSectionSize(64)
        self.l_qksz_name = QtGui.QLabel(Dialog)
        self.l_qksz_name.setGeometry(QtCore.QRect(10, 20, 131, 16))
        self.l_qksz_name.setObjectName(_fromUtf8("l_qksz_name"))
        self.l_qksz_data = QtGui.QLabel(Dialog)
        self.l_qksz_data.setGeometry(QtCore.QRect(140, 20, 81, 16))
        self.l_qksz_data.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.l_qksz_data.setObjectName(_fromUtf8("l_qksz_data"))
        self.l_yxsz_name = QtGui.QLabel(Dialog)
        self.l_yxsz_name.setGeometry(QtCore.QRect(260, 20, 111, 16))
        self.l_yxsz_name.setObjectName(_fromUtf8("l_yxsz_name"))
        self.l_yxsz_data = QtGui.QLabel(Dialog)
        self.l_yxsz_data.setGeometry(QtCore.QRect(370, 20, 81, 16))
        self.l_yxsz_data.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.l_yxsz_data.setObjectName(_fromUtf8("l_yxsz_data"))

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "替代补足", None))
        self.pb_plbz.setText(_translate("Dialog", "批量补足", None))
        self.pb_cancel.setText(_translate("Dialog", "取消", None))
        item = self.tv_data_list.horizontalHeaderItem(0)
        item.setText(_translate("Dialog", "勾选", None))
        item = self.tv_data_list.horizontalHeaderItem(1)
        item.setText(_translate("Dialog", "序号", None))
        item = self.tv_data_list.horizontalHeaderItem(2)
        item.setText(_translate("Dialog", "股票代码", None))
        item = self.tv_data_list.horizontalHeaderItem(3)
        item.setText(_translate("Dialog", "股票名称", None))
        item = self.tv_data_list.horizontalHeaderItem(4)
        item.setText(_translate("Dialog", "股票状态", None))
        item = self.tv_data_list.horizontalHeaderItem(5)
        item.setText(_translate("Dialog", "缺口数量", None))
        item = self.tv_data_list.horizontalHeaderItem(6)
        item.setText(_translate("Dialog", "缺口市值", None))
        self.l_qksz_name.setText(_translate("Dialog", "现金替代上限缺口市值：", None))
        self.l_qksz_data.setText(_translate("Dialog", "12,561.12", None))
        self.l_yxsz_name.setText(_translate("Dialog", "替代补足已选市值：", None))
        self.l_yxsz_data.setText(_translate("Dialog", "12,561.12", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog = QtGui.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

