# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'SpeedApplyRedeem_UseVolume.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(283, 299)
        self.pb_cancel = QtGui.QPushButton(Dialog)
        self.pb_cancel.setGeometry(QtCore.QRect(160, 250, 75, 23))
        self.pb_cancel.setObjectName(_fromUtf8("pb_cancel"))
        self.f_volume3 = QtGui.QFrame(Dialog)
        self.f_volume3.setGeometry(QtCore.QRect(30, 120, 231, 41))
        self.f_volume3.setFrameShape(QtGui.QFrame.StyledPanel)
        self.f_volume3.setFrameShadow(QtGui.QFrame.Raised)
        self.f_volume3.setObjectName(_fromUtf8("f_volume3"))
        self.le_volume3_2 = QtGui.QLineEdit(self.f_volume3)
        self.le_volume3_2.setGeometry(QtCore.QRect(130, 10, 81, 20))
        self.le_volume3_2.setObjectName(_fromUtf8("le_volume3_2"))
        self.l_volume3_2 = QtGui.QLabel(self.f_volume3)
        self.l_volume3_2.setGeometry(QtCore.QRect(20, 10, 54, 20))
        self.l_volume3_2.setObjectName(_fromUtf8("l_volume3_2"))
        self.f_volume4 = QtGui.QFrame(Dialog)
        self.f_volume4.setGeometry(QtCore.QRect(30, 160, 231, 41))
        self.f_volume4.setFrameShape(QtGui.QFrame.StyledPanel)
        self.f_volume4.setFrameShadow(QtGui.QFrame.Raised)
        self.f_volume4.setObjectName(_fromUtf8("f_volume4"))
        self.le_volume4_2 = QtGui.QLineEdit(self.f_volume4)
        self.le_volume4_2.setGeometry(QtCore.QRect(130, 10, 81, 20))
        self.le_volume4_2.setObjectName(_fromUtf8("le_volume4_2"))
        self.l_volume4_2 = QtGui.QLabel(self.f_volume4)
        self.l_volume4_2.setGeometry(QtCore.QRect(20, 10, 54, 20))
        self.l_volume4_2.setObjectName(_fromUtf8("l_volume4_2"))
        self.pb_enter = QtGui.QPushButton(Dialog)
        self.pb_enter.setGeometry(QtCore.QRect(50, 250, 75, 23))
        self.pb_enter.setObjectName(_fromUtf8("pb_enter"))
        self.f_volume1 = QtGui.QFrame(Dialog)
        self.f_volume1.setGeometry(QtCore.QRect(30, 40, 231, 41))
        self.f_volume1.setFrameShape(QtGui.QFrame.StyledPanel)
        self.f_volume1.setFrameShadow(QtGui.QFrame.Raised)
        self.f_volume1.setObjectName(_fromUtf8("f_volume1"))
        self.le_volume1_2 = QtGui.QLineEdit(self.f_volume1)
        self.le_volume1_2.setGeometry(QtCore.QRect(130, 10, 81, 20))
        self.le_volume1_2.setObjectName(_fromUtf8("le_volume1_2"))
        self.l_volume1_2 = QtGui.QLabel(self.f_volume1)
        self.l_volume1_2.setGeometry(QtCore.QRect(20, 10, 54, 20))
        self.l_volume1_2.setObjectName(_fromUtf8("l_volume1_2"))
        self.pb_useVolume = QtGui.QPushButton(Dialog)
        self.pb_useVolume.setGeometry(QtCore.QRect(30, 10, 91, 23))
        self.pb_useVolume.setObjectName(_fromUtf8("pb_useVolume"))
        self.f_volume2 = QtGui.QFrame(Dialog)
        self.f_volume2.setGeometry(QtCore.QRect(30, 80, 231, 41))
        self.f_volume2.setFrameShape(QtGui.QFrame.StyledPanel)
        self.f_volume2.setFrameShadow(QtGui.QFrame.Raised)
        self.f_volume2.setObjectName(_fromUtf8("f_volume2"))
        self.le_volume2_2 = QtGui.QLineEdit(self.f_volume2)
        self.le_volume2_2.setGeometry(QtCore.QRect(130, 10, 81, 20))
        self.le_volume2_2.setObjectName(_fromUtf8("le_volume2_2"))
        self.l_volume2_2 = QtGui.QLabel(self.f_volume2)
        self.l_volume2_2.setGeometry(QtCore.QRect(20, 10, 54, 20))
        self.l_volume2_2.setObjectName(_fromUtf8("l_volume2_2"))
        self.f_volume5 = QtGui.QFrame(Dialog)
        self.f_volume5.setGeometry(QtCore.QRect(30, 200, 231, 41))
        self.f_volume5.setFrameShape(QtGui.QFrame.StyledPanel)
        self.f_volume5.setFrameShadow(QtGui.QFrame.Raised)
        self.f_volume5.setObjectName(_fromUtf8("f_volume5"))
        self.le_volume5_2 = QtGui.QLineEdit(self.f_volume5)
        self.le_volume5_2.setGeometry(QtCore.QRect(130, 10, 81, 20))
        self.le_volume5_2.setObjectName(_fromUtf8("le_volume5_2"))
        self.l_volume5_2 = QtGui.QLabel(self.f_volume5)
        self.l_volume5_2.setGeometry(QtCore.QRect(20, 10, 54, 20))
        self.l_volume5_2.setObjectName(_fromUtf8("l_volume5_2"))

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "Dialog", None))
        self.pb_cancel.setText(_translate("Dialog", "取消", None))
        self.l_volume3_2.setText(_translate("Dialog", "委托数量3", None))
        self.l_volume4_2.setText(_translate("Dialog", "委托数量4", None))
        self.pb_enter.setText(_translate("Dialog", "确定", None))
        self.l_volume1_2.setText(_translate("Dialog", "委托数量1", None))
        self.pb_useVolume.setText(_translate("Dialog", "增加常用数量", None))
        self.l_volume2_2.setText(_translate("Dialog", "委托数量2", None))
        self.l_volume5_2.setText(_translate("Dialog", "委托数量5", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog = QtGui.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

