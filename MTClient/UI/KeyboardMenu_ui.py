# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'KeyboardMenu.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_DockWidget(object):
    def setupUi(self, DockWidget):
        DockWidget.setObjectName(_fromUtf8("DockWidget"))
        DockWidget.resize(877, 63)
        DockWidget.setFloating(False)
        self.dockWidgetContents = QtGui.QWidget()
        self.dockWidgetContents.setObjectName(_fromUtf8("dockWidgetContents"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.dockWidgetContents)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.pb_SpeedBuy = QtGui.QPushButton(self.dockWidgetContents)
        self.pb_SpeedBuy.setObjectName(_fromUtf8("pb_SpeedBuy"))
        self.horizontalLayout.addWidget(self.pb_SpeedBuy)
        self.pb_SpeedSell = QtGui.QPushButton(self.dockWidgetContents)
        self.pb_SpeedSell.setObjectName(_fromUtf8("pb_SpeedSell"))
        self.horizontalLayout.addWidget(self.pb_SpeedSell)
        self.pb_stock = QtGui.QPushButton(self.dockWidgetContents)
        self.pb_stock.setObjectName(_fromUtf8("pb_stock"))
        self.horizontalLayout.addWidget(self.pb_stock)
        self.pb_future = QtGui.QPushButton(self.dockWidgetContents)
        self.pb_future.setObjectName(_fromUtf8("pb_future"))
        self.horizontalLayout.addWidget(self.pb_future)
        self.pb_basis = QtGui.QPushButton(self.dockWidgetContents)
        self.pb_basis.setObjectName(_fromUtf8("pb_basis"))
        self.horizontalLayout.addWidget(self.pb_basis)
        self.pb_position = QtGui.QPushButton(self.dockWidgetContents)
        self.pb_position.setObjectName(_fromUtf8("pb_position"))
        self.horizontalLayout.addWidget(self.pb_position)
        self.pb_detail = QtGui.QPushButton(self.dockWidgetContents)
        self.pb_detail.setObjectName(_fromUtf8("pb_detail"))
        self.horizontalLayout.addWidget(self.pb_detail)
        self.pb_grade = QtGui.QPushButton(self.dockWidgetContents)
        self.pb_grade.setObjectName(_fromUtf8("pb_grade"))
        self.horizontalLayout.addWidget(self.pb_grade)
        self.pb_addKeyboard = QtGui.QPushButton(self.dockWidgetContents)
        self.pb_addKeyboard.setObjectName(_fromUtf8("pb_addKeyboard"))
        self.horizontalLayout.addWidget(self.pb_addKeyboard)
        spacerItem = QtGui.QSpacerItem(127, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        DockWidget.setWidget(self.dockWidgetContents)

        self.retranslateUi(DockWidget)
        QtCore.QMetaObject.connectSlotsByName(DockWidget)

    def retranslateUi(self, DockWidget):
        DockWidget.setWindowTitle(_translate("DockWidget", "快捷菜单", None))
        self.pb_SpeedBuy.setText(_translate("DockWidget", "快（F10）", None))
        self.pb_SpeedSell.setText(_translate("DockWidget", "篮（F10）", None))
        self.pb_stock.setText(_translate("DockWidget", "股（F11）", None))
        self.pb_future.setText(_translate("DockWidget", "期（F12）", None))
        self.pb_basis.setText(_translate("DockWidget", "基差（F5）", None))
        self.pb_position.setText(_translate("DockWidget", "持仓（F6）", None))
        self.pb_detail.setText(_translate("DockWidget", "明细（F7）", None))
        self.pb_grade.setText(_translate("DockWidget", "分级（F8）", None))
        self.pb_addKeyboard.setText(_translate("DockWidget", "+", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    DockWidget = QtGui.QDockWidget()
    ui = Ui_DockWidget()
    ui.setupUi(DockWidget)
    DockWidget.show()
    sys.exit(app.exec_())

