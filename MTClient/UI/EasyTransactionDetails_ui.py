# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'EasyTransactionDetails.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_d_easyTransactionDetails(object):
    def setupUi(self, d_easyTransactionDetails):
        d_easyTransactionDetails.setObjectName(_fromUtf8("d_easyTransactionDetails"))
        d_easyTransactionDetails.resize(680, 453)
        self.dockWidgetContents = QtGui.QWidget()
        self.dockWidgetContents.setObjectName(_fromUtf8("dockWidgetContents"))
        self.l_variety = QtGui.QLabel(self.dockWidgetContents)
        self.l_variety.setGeometry(QtCore.QRect(10, 10, 31, 16))
        self.l_variety.setObjectName(_fromUtf8("l_variety"))
        self.rb_variety_all = QtGui.QRadioButton(self.dockWidgetContents)
        self.rb_variety_all.setGeometry(QtCore.QRect(50, 10, 51, 16))
        self.rb_variety_all.setChecked(False)
        self.rb_variety_all.setObjectName(_fromUtf8("rb_variety_all"))
        self.rb_variety_stock = QtGui.QRadioButton(self.dockWidgetContents)
        self.rb_variety_stock.setGeometry(QtCore.QRect(100, 10, 51, 16))
        self.rb_variety_stock.setChecked(False)
        self.rb_variety_stock.setObjectName(_fromUtf8("rb_variety_stock"))
        self.rb_variety_other = QtGui.QRadioButton(self.dockWidgetContents)
        self.rb_variety_other.setGeometry(QtCore.QRect(150, 10, 51, 16))
        self.rb_variety_other.setChecked(False)
        self.rb_variety_other.setObjectName(_fromUtf8("rb_variety_other"))
        self.l_tradeTerminal = QtGui.QLabel(self.dockWidgetContents)
        self.l_tradeTerminal.setGeometry(QtCore.QRect(250, 10, 51, 16))
        self.l_tradeTerminal.setObjectName(_fromUtf8("l_tradeTerminal"))
        self.rb_tradeTerminal_ip = QtGui.QRadioButton(self.dockWidgetContents)
        self.rb_tradeTerminal_ip.setGeometry(QtCore.QRect(410, 10, 61, 16))
        self.rb_tradeTerminal_ip.setChecked(False)
        self.rb_tradeTerminal_ip.setObjectName(_fromUtf8("rb_tradeTerminal_ip"))
        self.rb_tradeTerminal_self = QtGui.QRadioButton(self.dockWidgetContents)
        self.rb_tradeTerminal_self.setGeometry(QtCore.QRect(360, 10, 51, 16))
        self.rb_tradeTerminal_self.setChecked(False)
        self.rb_tradeTerminal_self.setObjectName(_fromUtf8("rb_tradeTerminal_self"))
        self.rb_tradeTerminal_all = QtGui.QRadioButton(self.dockWidgetContents)
        self.rb_tradeTerminal_all.setGeometry(QtCore.QRect(310, 10, 51, 16))
        self.rb_tradeTerminal_all.setChecked(True)
        self.rb_tradeTerminal_all.setObjectName(_fromUtf8("rb_tradeTerminal_all"))
        self.le_search = QtGui.QLineEdit(self.dockWidgetContents)
        self.le_search.setGeometry(QtCore.QRect(470, 10, 113, 20))
        self.le_search.setObjectName(_fromUtf8("le_search"))
        self.pb_search = QtGui.QPushButton(self.dockWidgetContents)
        self.pb_search.setGeometry(QtCore.QRect(590, 10, 75, 20))
        self.pb_search.setObjectName(_fromUtf8("pb_search"))
        self.l_pageInfo = QtGui.QLabel(self.dockWidgetContents)
        self.l_pageInfo.setGeometry(QtCore.QRect(10, 410, 211, 16))
        self.l_pageInfo.setObjectName(_fromUtf8("l_pageInfo"))
        self.table_searchData = QtGui.QTableWidget(self.dockWidgetContents)
        self.table_searchData.setGeometry(QtCore.QRect(15, 40, 651, 361))
        self.table_searchData.setObjectName(_fromUtf8("table_searchData"))
        self.table_searchData.setColumnCount(6)
        self.table_searchData.setRowCount(0)
        item = QtGui.QTableWidgetItem()
        self.table_searchData.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.table_searchData.setHorizontalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.table_searchData.setHorizontalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        self.table_searchData.setHorizontalHeaderItem(3, item)
        item = QtGui.QTableWidgetItem()
        self.table_searchData.setHorizontalHeaderItem(4, item)
        item = QtGui.QTableWidgetItem()
        self.table_searchData.setHorizontalHeaderItem(5, item)
        self.table_searchData.horizontalHeader().setCascadingSectionResizes(False)
        self.table_searchData.horizontalHeader().setDefaultSectionSize(105)
        self.table_searchData.horizontalHeader().setSortIndicatorShown(False)
        self.table_searchData.horizontalHeader().setStretchLastSection(False)
        self.table_searchData.verticalHeader().setCascadingSectionResizes(False)
        self.table_searchData.verticalHeader().setDefaultSectionSize(30)
        self.table_searchData.verticalHeader().setStretchLastSection(False)
        self.pb_firstPage = QtGui.QPushButton(self.dockWidgetContents)
        self.pb_firstPage.setGeometry(QtCore.QRect(270, 410, 60, 20))
        self.pb_firstPage.setObjectName(_fromUtf8("pb_firstPage"))
        self.pb_gotoPage = QtGui.QPushButton(self.dockWidgetContents)
        self.pb_gotoPage.setGeometry(QtCore.QRect(460, 410, 60, 20))
        self.pb_gotoPage.setObjectName(_fromUtf8("pb_gotoPage"))
        self.pb_nextPage = QtGui.QPushButton(self.dockWidgetContents)
        self.pb_nextPage.setGeometry(QtCore.QRect(530, 410, 60, 20))
        self.pb_nextPage.setObjectName(_fromUtf8("pb_nextPage"))
        self.pb_lastPage = QtGui.QPushButton(self.dockWidgetContents)
        self.pb_lastPage.setGeometry(QtCore.QRect(600, 410, 60, 20))
        self.pb_lastPage.setObjectName(_fromUtf8("pb_lastPage"))
        self.pb_prePage = QtGui.QPushButton(self.dockWidgetContents)
        self.pb_prePage.setGeometry(QtCore.QRect(340, 410, 60, 20))
        self.pb_prePage.setObjectName(_fromUtf8("pb_prePage"))
        self.le_pageNumber = QtGui.QLineEdit(self.dockWidgetContents)
        self.le_pageNumber.setGeometry(QtCore.QRect(410, 410, 41, 20))
        self.le_pageNumber.setAlignment(QtCore.Qt.AlignCenter)
        self.le_pageNumber.setObjectName(_fromUtf8("le_pageNumber"))
        d_easyTransactionDetails.setWidget(self.dockWidgetContents)

        self.retranslateUi(d_easyTransactionDetails)
        QtCore.QMetaObject.connectSlotsByName(d_easyTransactionDetails)

    def retranslateUi(self, d_easyTransactionDetails):
        d_easyTransactionDetails.setWindowTitle(_translate("d_easyTransactionDetails", "简版成交明细", None))
        self.l_variety.setText(_translate("d_easyTransactionDetails", "品种", None))
        self.rb_variety_all.setText(_translate("d_easyTransactionDetails", "全部", None))
        self.rb_variety_stock.setText(_translate("d_easyTransactionDetails", "股票", None))
        self.rb_variety_other.setText(_translate("d_easyTransactionDetails", "其他", None))
        self.l_tradeTerminal.setText(_translate("d_easyTransactionDetails", "交易终端", None))
        self.rb_tradeTerminal_ip.setText(_translate("d_easyTransactionDetails", "终端IP", None))
        self.rb_tradeTerminal_self.setText(_translate("d_easyTransactionDetails", "自己", None))
        self.rb_tradeTerminal_all.setText(_translate("d_easyTransactionDetails", "全部", None))
        self.pb_search.setText(_translate("d_easyTransactionDetails", "查询", None))
        self.l_pageInfo.setText(_translate("d_easyTransactionDetails", "总计1270条，当前第1页/共113页", None))
        item = self.table_searchData.horizontalHeaderItem(0)
        item.setText(_translate("d_easyTransactionDetails", "证券代码", None))
        item = self.table_searchData.horizontalHeaderItem(1)
        item.setText(_translate("d_easyTransactionDetails", "成交时间", None))
        item = self.table_searchData.horizontalHeaderItem(2)
        item.setText(_translate("d_easyTransactionDetails", "累计成交量", None))
        item = self.table_searchData.horizontalHeaderItem(3)
        item.setText(_translate("d_easyTransactionDetails", "成交均价", None))
        item = self.table_searchData.horizontalHeaderItem(4)
        item.setText(_translate("d_easyTransactionDetails", "委托数量", None))
        item = self.table_searchData.horizontalHeaderItem(5)
        item.setText(_translate("d_easyTransactionDetails", "买卖方向", None))
        self.pb_firstPage.setText(_translate("d_easyTransactionDetails", "首页", None))
        self.pb_gotoPage.setText(_translate("d_easyTransactionDetails", "跳转", None))
        self.pb_nextPage.setText(_translate("d_easyTransactionDetails", "下一页", None))
        self.pb_lastPage.setText(_translate("d_easyTransactionDetails", "末页", None))
        self.pb_prePage.setText(_translate("d_easyTransactionDetails", "上一页", None))
        self.le_pageNumber.setText(_translate("d_easyTransactionDetails", "1", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    d_easyTransactionDetails = QtGui.QDockWidget()
    ui = Ui_d_easyTransactionDetails()
    ui.setupUi(d_easyTransactionDetails)
    d_easyTransactionDetails.show()
    sys.exit(app.exec_())

