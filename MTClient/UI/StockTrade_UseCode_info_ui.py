# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'StockTrade_UseCode_info.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_d_info(object):
    def setupUi(self, d_info):
        d_info.setObjectName(_fromUtf8("d_info"))
        d_info.resize(212, 90)
        self.pb_enter = QtGui.QPushButton(d_info)
        self.pb_enter.setGeometry(QtCore.QRect(20, 50, 75, 23))
        self.pb_enter.setObjectName(_fromUtf8("pb_enter"))
        self.pb_cancel = QtGui.QPushButton(d_info)
        self.pb_cancel.setGeometry(QtCore.QRect(120, 50, 75, 23))
        self.pb_cancel.setObjectName(_fromUtf8("pb_cancel"))
        self.label = QtGui.QLabel(d_info)
        self.label.setGeometry(QtCore.QRect(30, 20, 181, 16))
        self.label.setObjectName(_fromUtf8("label"))

        self.retranslateUi(d_info)
        QtCore.QObject.connect(self.pb_cancel, QtCore.SIGNAL(_fromUtf8("clicked()")), d_info.close)
        QtCore.QMetaObject.connectSlotsByName(d_info)

    def retranslateUi(self, d_info):
        d_info.setWindowTitle(_translate("d_info", "信息提示", None))
        self.pb_enter.setText(_translate("d_info", "确定", None))
        self.pb_cancel.setText(_translate("d_info", "取消", None))
        self.label.setText(_translate("d_info", "常用代码设置数量最多为10", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    d_info = QtGui.QDialog()
    ui = Ui_d_info()
    ui.setupUi(d_info)
    d_info.show()
    sys.exit(app.exec_())

