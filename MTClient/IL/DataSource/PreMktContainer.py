#!/usr/bin/env python
# -*- coding: utf-8 -*-

from IL.Socket.Singleton import singleton


@singleton
class Memory:
    def __init__(self):
        self.MarketDaily = dict()
        self.EtfBasketInfo = {
            u'1To2': dict(),
            u'2To1': dict(),
            u'2ToEtfData': dict(),
            u'2ToEtfList': dict()
        }
        self.StructuredFundInfo = dict()
        self.structured_fund = {
            u'AToCode': dict(),
            u'BToCode': dict(),
            u'MToCode': dict(),
            u'MtoData': dict()
        }
        self.EtfListDiffItem = {
            u'2ToEtfData': dict(),
            u'2ToEtfList': dict()
        }
        self.TradedayMsg = {
            u'trade': dict(),
            u'next': dict(),
            u'pre': dict(),
            u'weekday': dict()
        }

    def unitest_print(self):
        print 'TradeDay', len(self.TradedayMsg['trade'])
        print '2ToEtfList', len(self.EtfBasketInfo['2ToEtfList'])
        print '2ToEtfData', len(self.EtfListDiffItem['2ToEtfData'])
        print 'structured_fund', len(self.structured_fund['MToCode'])
        print 'StructuredFundInfo', len(self.StructuredFundInfo)
        print 'MarketDaily', len(self.MarketDaily)

    def get_TradedayMsg(self, day):
        if self.TradedayMsg['trade'].get(day, None) is None or self.TradedayMsg['trade'][day] is 0:
            return []
        return [self.TradedayMsg['pre'][day], self.TradedayMsg['next'][day], self.TradedayMsg['weekday'][day]]

    def add_TradedayMsg(self, msg):
        self.TradedayMsg['trade'][msg.cur_tradeday] = msg.flag
        self.TradedayMsg['next'][msg.cur_tradeday] = msg.next_tradeday
        self.TradedayMsg['pre'][msg.cur_tradeday] = msg.pre_tradeday
        self.TradedayMsg['weekday'][msg.cur_tradeday] = msg.weekday

    def get_EtfListDiff(self, etfcode):
        return self.EtfListDiffItem['2ToEtfData'].get(etfcode, None)

    def get_EtfListDiff_List(self, etfcode):
        ret = self.EtfListDiffItem['2ToEtfData'].get(etfcode, None)
        if not ret:
            return []
        return ret.values()

    def get_EtfListDiff_Specify(self, etfcode, stockcode):
        ret = self.EtfListDiffItem['2ToEtfData'].get(etfcode, None)
        if not ret:
            return None
        return ret.get(stockcode, None)

    def add_EtfListDiff(self, msg):
        self.EtfListDiffItem['2ToEtfData'][msg.etfcode] = msg
        if self.EtfListDiffItem['2ToEtfList'].get(msg.etfcode, None) is None:
            self.EtfListDiffItem['2ToEtfList'][msg.etfcode] = dict()
        for i in msg.item:
            self.EtfListDiffItem['2ToEtfList'][msg.etfcode][i.stk_id] = i

    def get_structured_fund_a(self, code):
        ret = self.structured_fund['AToCode'].get(code, None)
        if ret is None:
            return []
        ret.append(self.structured_fund['MtoData'][ret[0]])
        return ret

    def get_structured_fund_b(self, code):
        ret = self.structured_fund['BToCode'].get(code, None)
        if ret is None:
            return []
        ret.append(self.structured_fund['MtoData'][ret[0]])
        return ret

    def get_structured_fund_m(self, code):
        ret = self.structured_fund['MToCode'].get(code, None)
        if ret is None:
            return []
        ret.append(self.structured_fund['MtoData'][ret[0]])
        return ret

    def get_structured_fund_codes(self, code):
        ret = self.structured_fund['MToCode'].get(code, None)
        if ret:
            return ret
        ret = self.structured_fund['AToCode'].get(code, None)
        if ret:
            return ret
        ret = self.structured_fund['BToCode'].get(code, None)
        return ret

    def add_structured_fund(self, msg):
        self.structured_fund['AToCode'][msg.a_stk_id] = [msg.m_stk_id, msg.a_stk_id, msg.b_stk_id]
        self.structured_fund['BToCode'][msg.b_stk_id] = [msg.m_stk_id, msg.a_stk_id, msg.b_stk_id]
        self.structured_fund['MToCode'][msg.m_stk_id] = [msg.m_stk_id, msg.a_stk_id, msg.b_stk_id]
        self.structured_fund['MtoData'][msg.m_stk_id] = msg

    def get_StructuredFundInfo(self, code):
        return self.StructuredFundInfo.get(code, None)

    def add_StructuredFundInfo(self, msg):
        self.StructuredFundInfo[msg.stock_id] = msg

    def get_ETF_CodeList(self, code):
        if self.EtfBasketInfo['2ToEtfList'].get(code, None) is None:
            return []
        else:
            return list(self.EtfBasketInfo['2ToEtfList'][code]['EtfListCodeSet'])

    def get_IsETF(self, code):
        if not self.EtfBasketInfo['1To2'].get(code, None) or not self.EtfBasketInfo['2To1'].get(code, None):
            return True
        else:
            return False

    def get_ETF_DataList(self, code):
        if self.EtfBasketInfo['2ToEtfList'].get(code, None) is None:
            return []
        else:
            return self.EtfBasketInfo['2ToEtfList'][code]['EtfListCodeToEtfListData']

    def get_EtfBasketInfo(self, code):
        return self.EtfBasketInfo['2ToEtfData'].get(code, None)

    def get_ETF_1to2(self, code):
        return self.EtfBasketInfo['1To2'].get(code, None)

    def get_ETF_2To1(self, code):
        return self.EtfBasketInfo['2To1'].get(code, None)

    def add_EtfBasketInfo(self, msg):
        self.EtfBasketInfo['1To2'][msg.etf_p_r_code] = msg.etf_code
        self.EtfBasketInfo['2To1'][msg.etf_code] = msg.etf_p_r_code
        self.EtfBasketInfo['2ToEtfData'][msg.etf_code] = msg
        if self.EtfBasketInfo['2ToEtfList'].get(msg.etf_code, None) is None:
            self.EtfBasketInfo['2ToEtfList'][msg.etf_code] = dict()
            self.EtfBasketInfo['2ToEtfList'][msg.etf_code]['EtfListCodeSet'] = set()
            self.EtfBasketInfo['2ToEtfList'][msg.etf_code]['EtfListCodeToEtfListData'] = dict()

        for i in msg.etf_list:
            self.EtfBasketInfo['2ToEtfList'][msg.etf_code]['EtfListCodeSet'].add(i.stock_id)
            self.EtfBasketInfo['2ToEtfList'][msg.etf_code]['EtfListCodeToEtfListData'][i.stock_id] = i

    def get_MarketDaily(self, code):
        return self.MarketDaily.get(code, None)

    def add_MarketDaily(self, msg):
        self.MarketDaily[msg.stock_id] = msg
