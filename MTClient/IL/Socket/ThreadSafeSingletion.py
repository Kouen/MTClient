#!/usr/bin/env python
# -*- coding: utf-8 -*-

import Singleton
from Queue import Queue


@Singleton.singleton
class SafeQueue():
    def __init__(self):
        self.queue = Queue()

    def size(self):
        return self.queue.qsize()

    def put(self, data):
        self.queue.put(data)

    def get(self):
        res = self.queue.get(timeout=30)
        return res


if __name__ == "__main__":
    a = SafeQueue()
    a.put([10, 7, "hh"])
    a.put("hello")
    a.size()
    a.get()
    a.get()
    a.size()