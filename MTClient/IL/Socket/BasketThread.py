#!/usr/bin/env python
# -*- coding: utf-8 -*-

import ThreadSafeSingletion
import threading
from IL.DataSource.Market import MarketSource
from IL.DataSource.Container import Memory
from IL.DataSource import PreMktContainer
import Robot
import gevent

class Distribute(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.queue = ThreadSafeSingletion.SafeQueue()
        self.market = MarketSource()
        self.dataContainer = Memory()
        self.premkt = PreMktContainer.Memory()
        self.robot = Robot.Connection()

    def run(self):
        print '[Start]Distribute'
        switch = {
            u'Login': self.Login,
            u'SingleOrder': self.SingleOrder,
            u'BasketOrder': self.BasketOrder,
            u'BasketWithdrawalResp': self.BasketWithdrawalResp,
            u'BasketUnfinished': self.BasketUnfinished,
            u'GradedFund': self.GradedFund,
            u'SubEtfInfo': self.SubEtfInfo,
            u'SubEtfInfo_LimitUp': self.SubEtfInfo_LimitUp,
            u'SubEtfInfo_LimitDown': self.SubEtfInfo_LimitDown,
            u'SubEtfInfo_Stop': self.SubEtfInfo_Stop,
            u'SubEtfInfo_MaxPrice': self.SubEtfInfo_MaxPrice,
            u'SubEtfInfo_MinPrice': self.SubEtfInfo_MinPrice,
            u'SubEtfInfo_DIOPVB1': self.SubEtfInfo_DIOPVB1,
            u'SubEtfInfo_DIOPVS1': self.SubEtfInfo_DIOPVS1,
            u'SubEtfInfo_Basis_B1': self.SubEtfInfo_Basis_B1,
            u'SubEtfInfo_Basis_S1': self.SubEtfInfo_Basis_S1,
            u'SubEtfInfo_Basis_Open': self.SubEtfInfo_Basis_Open,
            u'SubEtfInfo_Basis_Close': self.SubEtfInfo_Basis_Close,

            u'StockCodeTable': self.CodeTable,
            u'FuturesCodeTable': self.FutureCodeTable,
            u'StockMarket': self.Stock,
            u'FuturesMarket': self.Future,
            u'RTS3Index': self.RTS3Index,

            # u'BasketPolicy': self.BasketPolicy,

            u'MarketOverview': self.MarketOverview,
            u'EtfBasketInfo': self.EtfBasketInfo,
            u'StructuredFundInfo': self.StructuredFundInfo,
            u'structured_fund': self.structured_fund,
            u'EtfListDiff': self.EtfListDiff,
            u'TradedayMsg': self.TradedayMsg,

            u'SS_Money': self.SS_Money,
            u'SS_Money_sub': self.SS_Money_sub,
            u'SS_Trade': self.SS_Trade,
            u'SS_Order': self.SS_Order,
            u'SS_Posit': self.SS_Posit,
            u'SS_SingerOrder': self.SS_SingleOrder
        }
        while True:
            try:
                msg = self.queue.get()
                switch[msg[0]](msg[1], msg[2])

                if msg[0] == 'Login' or \
                   msg[0] == 'SingleOrder' or \
                   msg[0] == 'BasketOrder' or \
                   msg[0] == 'BasketWithdrawalResp' or \
                   msg[0] == 'BasketUnfinished' or \
                   msg[0] == 'GradedFund' or \
                   msg[0] == 'SubEtfInfo':
                    self.robot.ExecuteCallBack(msg)
            except:
                pass


    def SS_SingleOrder(self, msg, requestID):
        if msg:
            self.dataContainer.add_orderSeq(msg, requestID)

    def SS_Money_sub(self, msg, requestID):
        if msg:
            self.dataContainer.add_Money_SS_Sub(msg)
    def SS_Money(self, msg, requestID):
        if msg:
            self.dataContainer.add_Money_SS(msg)

    def SS_Trade(self, msg, requestID):
        if msg:
            self.dataContainer.add_TradeDetail_SS(msg)

    def SS_Order(self, msg, requestID):
        if msg:
            self.dataContainer.add_orderInfo_SS(msg)

    def SS_Posit(self, msg, requestID):
        if msg:
            self.dataContainer.add_Position_SS(msg)

    def MarketOverview(self, msg, requestID):
        if msg:
            self.premkt.add_MarketDaily(msg)

    def EtfBasketInfo(self, msg, requestID):
        if msg:
            self.premkt.add_EtfBasketInfo(msg)

    def StructuredFundInfo(self, msg, requestID):
        if msg:
            self.premkt.add_StructuredFundInfo(msg)

    def structured_fund(self, msg, requestID):
        if msg:
            self.premkt.add_structured_fund(msg)

    def EtfListDiff(self, msg, requestID):
        if msg:
            self.premkt.add_EtfListDiff(msg)

    def TradedayMsg(self, msg, requestID):
        if msg:
            self.premkt.add_TradedayMsg(msg)

    def RTS3Index(self, msg, requestID):
        if msg:
            self.market.set(code=msg.idnum, market=msg)

    def Stock(self, msg, requestID):
        if msg:
            self.market.set(code=msg.idnum, market=msg)

    def CodeTable(self, msg, requestID):
        if msg:
            for i in msg.security_code_list:
                self.market.setCodeInfo(code=i.ch_security_code, data=i)

    def Future(self, msg, requestID):
        if msg:
            self.market.set(code=msg.idnum, market=msg)

    def FutureCodeTable(self, msg, requestID):
        if msg:
            for i in msg.security_code_list:
                self.market.setCodeInfo(code=i.ch_security_code, data=i)


    def SingleOrder(self, msg, requestID):
        if msg:
            self.dataContainer.add_orderSeq(msg, requestID)


    def Login(self, msg, requestID):
        if msg:
            self.dataContainer.add_LoginInfo(msg)

    def StockPosition(self, msg, requestID):
        if msg:
            self.dataContainer.add_Position(msg)

    def Money(self, msg, requestID):
        if msg:
            self.dataContainer.add_Money(msg)

    def TradeDetail(self, msg, requestID):
        if msg:
            self.dataContainer.add_TradeDetail(msg)

    def OrderSearch(self, msg, requestID):
        if msg:
            self.dataContainer.add_orderInfo(msg)

    def BasketUnfinished(self, msg, requestID):
        if msg:
            self.dataContainer.add_BasketUnfinished(msg)

    def TotalCash(self, msg, requestID):
        if msg:
            self.dataContainer.add_TotalCash(msg)

    def BasketPolicy(self, msg, requestID):
        if msg:
            self.dataContainer.add_BasketPolicy(msg, requestID)

    def BasketOrder(self, msg, requestID):
        if msg:
            self.dataContainer.update_BasketInfo(msg)

    def BasketWithdrawalResp(self, msg, requestID):
        pass

    def GradedFund(self, msg, requestID):
        if msg:
            self.dataContainer.add_PolicyResult(msg)

    def SubEtfInfo(self, msg, requestID):
        if msg:
            self.dataContainer.add_sub_key_etf_info(msg)

    def SubEtfInfo_LimitUp(self, msg, requestID):
        if msg:
            self.dataContainer.update_EtfInfo(msg, 'LimitUp')

    def SubEtfInfo_LimitDown(self, msg, requestID):
        if msg:
            self.dataContainer.update_EtfInfo(msg, 'LimitDow')

    def SubEtfInfo_Stop(self, msg, requestID):
        if msg:
            self.dataContainer.update_EtfInfo(msg, 'Stop')

    def SubEtfInfo_MaxPrice(self, msg, requestID):
        if msg:
            self.dataContainer.update_EtfInfo(msg, 'MaxPrice')

    def SubEtfInfo_MinPrice(self, msg, requestID):
        if msg:
            self.dataContainer.update_EtfInfo(msg, 'MinPrice')

    def SubEtfInfo_DIOPVB1(self, msg, requestID):
        if msg:
            self.dataContainer.update_EtfInfo(msg, 'DIOPVB1')

    def SubEtfInfo_DIOPVS1(self, msg, requestID):
        if msg:
            self.dataContainer.update_EtfInfo(msg, 'DIOPVS1')

    def SubEtfInfo_Basis_B1(self, msg, requestID):
        if msg:
            self.dataContainer.update_EtfInfo(msg, 'Basis_B1')

    def SubEtfInfo_Basis_S1(self, msg, requestID):
        if msg:
            self.dataContainer.update_EtfInfo(msg, 'Basis_S1')

    def SubEtfInfo_Basis_Open(self, msg, requestID):
        if msg:
            self.dataContainer.update_EtfInfo(msg, 'Basis_Open')

    def SubEtfInfo_Basis_Close(self, msg, requestID):
        if msg:
            self.dataContainer.update_EtfInfo(msg, 'Basis_Close')