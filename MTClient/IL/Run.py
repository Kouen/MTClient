#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import time
from IL.Socket.UnilMarket import *
from IL.Socket import stock_trade_pb2, trade_db_model_pb2,ConfigFileData, BasketThread, Robot, ot_pb2, pre_mkt, StockServerClient, base_pb2
from IL.Socket.pageType import *
import IL.DataSource.Container
import IL.DataSource.Market
import IL.DataSource.PreMktContainer
import gevent
import threading
import IL.Socket.Singleton

"""
logging.basicConfig(level=logging.DEBUG,
                    format=u'%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
                    datefmt=u'%a, %d %b %Y %H:%M:%S',
                    filename=u'../../Log/MTClient-{0}.log'.format(time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime())),
                    filemode=u'w')
"""

success = dict()
def RespCallBack(**kwargs):
    global success
    success[kwargs['requestID']][1] = True


def Unitest():
    time.sleep(1)

    trade_id = '192.168.1.116'
    trade_ip = '192.168.1.116'
    policy_id = ''
    robot = Robot.Connection()
    unitest_count = 0

    global success

    print u"*******[Start] Unitest*******"

    # CMD_LOGIN_REQ
    msg = base_pb2.LoginReq()
    msg.type = base_pb2.MANUAL_TRADE
    msg.trader_id = trade_id
    msg.version = base_pb2.MAJOR
    success[robot.send(base_pb2.CMD_LOGIN_REQ, msg, RespCallBack)] = ['CMD_LOGIN_REQ', False]
    unitest_count += 1

    # CMD_SINGLE_ORDER_REQ
    #   买SZ-深证华强
    msg = stock_trade_pb2.SingleOrderReq()
    msg.code = '000062'
    msg.price = 101000
    msg.qty = 1000
    msg.bs_flag = base_pb2.OPR_BUY
    msg.market = 0
    msg.trader_id = trade_id
    msg.trader_ip = trade_ip
    msg.instrument_type = 0x10
    msg.basket_amount = 0
    msg.policy_id = policy_id
    success[robot.send(base_pb2.CMD_SINGLE_ORDER_REQ, msg, RespCallBack)] = ['CMD_SINGLE_ORDER_REQ-BUY SZ', False]
    unitest_count += 1

    #   卖SZ-深证华强
    msg = stock_trade_pb2.SingleOrderReq()
    msg.code = '000062'
    msg.price = 101000
    msg.qty = 100
    msg.bs_flag = base_pb2.OPR_SELL
    msg.market = 0
    msg.trader_id = trade_id
    msg.trader_ip = trade_ip
    msg.instrument_type = 0x10
    msg.basket_amount = 0
    msg.policy_id = policy_id
    success[robot.send(base_pb2.CMD_SINGLE_ORDER_REQ, msg, RespCallBack)] = ['CMD_SINGLE_ORDER_REQ-SELL SZ', False]
    unitest_count += 1

    #   买SH-平安银行
    msg = stock_trade_pb2.SingleOrderReq()
    msg.code = '000001'
    msg.price = 101000
    msg.qty = 1000
    msg.bs_flag = base_pb2.OPR_BUY
    msg.market = 1
    msg.trader_id = trade_id
    msg.trader_ip = trade_ip
    msg.instrument_type = 0x10
    msg.basket_amount = 0
    msg.policy_id = ''
    success[robot.send(base_pb2.CMD_SINGLE_ORDER_REQ, msg, RespCallBack)] = ['CMD_SINGLE_ORDER_REQ-BUY SH', False]
    unitest_count += 1

    #   卖SH-平安银行
    msg = stock_trade_pb2.SingleOrderReq()
    msg.code = '000001'
    msg.price = 101000
    msg.qty = 100
    msg.bs_flag = base_pb2.OPR_SELL
    msg.market = 1
    msg.trader_id = trade_id
    msg.trader_ip = trade_ip
    msg.instrument_type = 0x10
    msg.basket_amount = 0
    msg.policy_id = policy_id
    success[robot.send(base_pb2.CMD_SINGLE_ORDER_REQ, msg, RespCallBack)] = ['CMD_SINGLE_ORDER_REQ-SELL SH', False]
    unitest_count += 1

    # CMD_SINGLE_WITHDRAWAL_REQ
    #   撤销SZ
    msg = stock_trade_pb2.SingleCancelReq()
    msg.order_no = '1'  # 不存在相关订单，只能发送假的订单号
    msg.market = 0
    msg.policy_id = policy_id
    msg.trader_id = trade_id
    msg.trader_ip = trade_ip
    success[robot.send(base_pb2.CMD_SINGLE_WITHDRAWAL_REQ, msg, RespCallBack)] = ['CMD_SINGLE_WITHDRAWAL_REQ-SZ', False]
    unitest_count += 1

    #   撤销SH
    msg = stock_trade_pb2.SingleCancelReq()
    msg.order_no = '2'  # 不存在相关订单，只能发送假的订单号
    msg.market = 1
    msg.policy_id = policy_id
    msg.trader_id = trade_id
    msg.trader_ip = trade_ip
    success[robot.send(base_pb2.CMD_SINGLE_ORDER_REQ, msg, RespCallBack)] = ['CMD_SINGLE_WITHDRAWAL_REQ-SH', False]
    unitest_count += 1

    # CMD_BASKET_ORDER_REQ
    #   买篮子
    msg = ot_pb2.BasketPolicyReq()
    msg.code = '510050'
    msg.pl = base_pb2.B_1
    msg.qty = 1
    msg.opr = base_pb2.OPR_BUY
    msg.policy_id = policy_id
    success[robot.send(base_pb2.CMD_BASKET_ORDER_REQ, msg, RespCallBack)] = ['CMD_BASKET_ORDER_REQ 买篮子', False]
    unitest_count += 1

    #   卖篮子
    msg = ot_pb2.BasketPolicyReq()
    msg.code = '510050'
    msg.pl = base_pb2.S_1
    msg.qty = 1
    msg.opr = base_pb2.OPR_SELL
    msg.policy_id = policy_id
    success[robot.send(base_pb2.CMD_BASKET_ORDER_REQ, msg, RespCallBack)] = ['CMD_BASKET_ORDER_REQ 卖篮子', False]
    unitest_count += 1

    #   篮子补单
    msg = ot_pb2.BasketPolicyReq()
    msg.code = '510050'
    msg.pl = base_pb2.B_1
    msg.qty = 1
    msg.opr = base_pb2.OPR_BASEKET_SUBMIT
    msg.policy_id = policy_id if policy_id == '' else '1'
    success[robot.send(base_pb2.CMD_BASKET_ORDER_REQ, msg, RespCallBack)] = ['CMD_BASKET_ORDER_REQ 篮子补单', False]
    unitest_count += 1

    #   篮子补齐
    msg = ot_pb2.BasketPolicyReq()
    msg.code = '510050'
    msg.pl = base_pb2.B_1
    msg.qty = 1
    msg.opr = base_pb2.OPR_BASEKET_COMPLETE
    msg.policy_id = policy_id if policy_id == '' else '1'
    success[robot.send(base_pb2.CMD_BASKET_ORDER_REQ, msg, RespCallBack)] = ['CMD_BASKET_ORDER_REQ 篮子补齐', False]
    unitest_count += 1

    #   自动撤补
    msg = ot_pb2.BasketPolicyReq()
    msg.code = '510050'
    msg.pl = base_pb2.B_1
    msg.qty = 1
    msg.opr = base_pb2.OPR_BASEKET_AUTO
    msg.policy_id = policy_id if policy_id == '' else '1'
    success[robot.send(base_pb2.CMD_BASKET_ORDER_REQ, msg, RespCallBack)] = ['CMD_BASKET_ORDER_REQ 篮子补齐', False]
    unitest_count += 1

    # CMD_BASKET_WITHDRAWAL_REQ
    msg = ot_pb2.BasketWithdrawalReq()
    msg.policy_id = policy_id if policy_id == '' else '1'
    success[robot.send(base_pb2.CMD_BASKET_WITHDRAWAL_REQ, msg, RespCallBack)] = ['CMD_BASKET_WITHDRAWAL_REQ', False]
    unitest_count += 1

    # CMD_BASKET_CUSTOM_REQ
    msg = ot_pb2.BasketCustomReq()
    son = msg.stocks.add()
    son.code = '000001'
    son.price = 101000
    son.qty = 1000
    son.bs_flag = base_pb2.OPR_BUY
    son.market = 1
    son.trader_id = trade_id
    son.trader_ip = trade_ip
    son.instrument_type = 0x10
    son.basket_amount = 0
    son.policy_id = ''

    son = msg.stocks.add()
    son.code = '000002'
    son.price = 201000
    son.qty = 2000
    son.bs_flag = base_pb2.OPR_BUY
    son.market = 1
    son.trader_id = trade_id
    son.trader_ip = trade_ip
    son.instrument_type = 0x10
    son.basket_amount = 0
    son.policy_id = ''

    son = msg.stocks.add()
    son.code = '000003'
    son.price = 81000
    son.qty = 3000
    son.bs_flag = base_pb2.OPR_BUY
    son.market = 1
    son.trader_id = trade_id
    son.trader_ip = trade_ip
    son.instrument_type = 0x10
    son.basket_amount = 0
    son.policy_id = ''
    success[robot.send(base_pb2.CMD_BASKET_CUSTOM_REQ, msg, RespCallBack)] = ['CMD_BASKET_CUSTOM_REQ', False]
    unitest_count += 1

    # CMD_UNFINISH_BASKET_REQ
    success[robot.send(base_pb2.CMD_UNFINISH_BASKET_REQ, None, RespCallBack)] = ['CMD_UNFINISH_BASKET_REQ', False]
    unitest_count += 1

    # CMD_GRADED_FUND_REQ
    #   分级基金合并
    msg = ot_pb2.GradingFundReq()
    msg.opr = base_pb2.OPR_COMBINE
    msg.code_a = '502049'
    msg.code_b = '502050'
    msg.code_m = '502048'
    msg.qty_m = 1
    msg.rate_a = 4
    msg.rate_b = 6
    msg.pl = base_pb2.PRICE_2_PERCENT
    success[robot.send(base_pb2.CMD_GRADED_FUND_REQ, msg, RespCallBack)] = ['CMD_GRADED_FUND_REQ 分级基金合并', False]
    unitest_count += 1

    #   分级基金拆分
    msg = ot_pb2.GradingFundReq()
    msg.opr = base_pb2.OPR_SPLIT
    msg.code_a = '502049'
    msg.code_b = '502050'
    msg.code_m = '502048'
    msg.qty_m = 1
    msg.rate_a = 4
    msg.rate_b = 6
    msg.pl = base_pb2.PRICE_2_PERCENT
    success[robot.send(base_pb2.CMD_GRADED_FUND_REQ, msg, RespCallBack)] = ['CMD_GRADED_FUND_REQ 分级基金拆分', False]
    unitest_count += 1

    #   分级基金买+合
    msg = ot_pb2.GradingFundReq()
    msg.opr = base_pb2.OPR_BUY_COMBINE
    msg.code_a = '502049'
    msg.code_b = '502050'
    msg.code_m = '502048'
    msg.qty_m = 1
    msg.rate_a = 4
    msg.rate_b = 6
    msg.pl = base_pb2.PRICE_2_PERCENT
    success[robot.send(base_pb2.CMD_GRADED_FUND_REQ, msg, RespCallBack)] = ['CMD_GRADED_FUND_REQ 分级基金买+合', False]
    unitest_count += 1

    #   分级基金快和
    msg = ot_pb2.GradingFundReq()
    msg.opr = base_pb2.OPR_FAST_COMBINE
    msg.code_a = '502049'
    msg.code_b = '502050'
    msg.code_m = '502048'
    msg.qty_m = 1
    msg.rate_a = 4
    msg.rate_b = 6
    msg.pl = base_pb2.PRICE_2_PERCENT
    success[robot.send(base_pb2.CMD_GRADED_FUND_REQ, msg, RespCallBack)] = ['CMD_GRADED_FUND_REQ 分级基金快和', False]
    unitest_count += 1

    #   分级基金拆+卖
    msg = ot_pb2.GradingFundReq()
    msg.opr = base_pb2.OPR_SPLIT_SELL
    msg.code_a = '502049'
    msg.code_b = '502050'
    msg.code_m = '502048'
    msg.qty_m = 1
    msg.rate_a = 4
    msg.rate_b = 6
    msg.pl = base_pb2.PRICE_2_PERCENT
    success[robot.send(base_pb2.CMD_GRADED_FUND_REQ, msg, RespCallBack)] = ['CMD_GRADED_FUND_REQ 分级基金拆+卖', False]
    unitest_count += 1

    #   分级基金快拆
    msg = ot_pb2.GradingFundReq()
    msg.opr = base_pb2.OPR_FAST_SPLIT
    msg.code_a = '502049'
    msg.code_b = '502050'
    msg.code_m = '502048'
    msg.qty_m = 1
    msg.rate_a = 4
    msg.rate_b = 6
    msg.pl = base_pb2.PRICE_2_PERCENT
    success[robot.send(base_pb2.CMD_GRADED_FUND_REQ, msg, RespCallBack)] = ['CMD_GRADED_FUND_REQ 分级基金快拆', False]
    unitest_count += 1

    # CMD_SUB_ETF_INFO_REQ
    #   info
    msg = ot_pb2.EtfInfoSubReq()
    msg.code = '510030'
    msg.sub = base_pb2.SUB_ETF_INFO
    success[robot.send(base_pb2.CMD_SUB_ETF_INFO_REQ, msg, RespCallBack)] = ['CMD_SUB_ETF_INFO_REQ 订阅ETF基本信息', False]
    unitest_count += 1

    #   basis
    msg = ot_pb2.EtfInfoSubReq()
    msg.code = '510030'
    msg.sub = base_pb2.SUB_ETF_BASIS
    success[robot.send(base_pb2.CMD_SUB_ETF_INFO_REQ, msg, RespCallBack)] = ['CMD_SUB_ETF_INFO_REQ 订阅ETF基差', False]
    unitest_count += 1

    #   停止订阅
    msg = ot_pb2.EtfInfoSubReq()
    msg.code = '510030'
    msg.sub = base_pb2.SUB_CLOSE
    success[robot.send(base_pb2.CMD_SUB_ETF_INFO_REQ, msg, RespCallBack)] = ['CMD_SUB_ETF_INFO_REQ 停止订阅', False]
    unitest_count += 1

    # CMD_SUB_CUSTOM_BASIS_REQ
    msg = ot_pb2.CustomBasisSubReq()
    son = msg.stocks.add()
    son.code = '000001'
    son.qty = 1000

    son = msg.stocks.add()
    son.code = '000002'
    son.qty = 1100
    success[robot.send(base_pb2.CMD_SUB_CUSTOM_BASIS_REQ, msg, RespCallBack)] = ['CMD_SUB_CUSTOM_BASIS_REQ 订阅自定义基差', False]
    unitest_count += 1

    # CMD_ETF_ORDER_REQ
    #   申购-深证
    msg = stock_trade_pb2.SingleOrderReq()
    msg.code = '399330'
    msg.price = 21111
    msg.qty = 1
    msg.bs_flag = base_pb2.OPR_PURCHASE
    msg.market = 0
    msg.trader_id = trade_id
    msg.trader_ip = trade_ip
    msg.instrument_type = 0x40
    msg.basket_amount = msg.price * msg.qty
    msg.policy_id = policy_id
    success[robot.send(base_pb2.CMD_ETF_ORDER_REQ, msg, RespCallBack)] = ['CMD_ETF_ORDER_REQ 申购-深证', False]
    unitest_count += 1

    #   赎回-深证
    msg = stock_trade_pb2.SingleOrderReq()
    msg.code = '399330'
    msg.price = 21111
    msg.qty = 1
    msg.bs_flag = base_pb2.OPR_REDEEM
    msg.market = 0
    msg.trader_id = trade_id
    msg.trader_ip = trade_ip
    msg.instrument_type = 0x40
    msg.basket_amount = msg.price * msg.qty
    msg.policy_id = policy_id
    success[robot.send(base_pb2.CMD_ETF_ORDER_REQ, msg, RespCallBack)] = ['CMD_ETF_ORDER_REQ 赎回-深证', False]
    unitest_count += 1

    #   申购-上海
    msg = stock_trade_pb2.SingleOrderReq()
    msg.code = '510030'
    msg.price = 21111
    msg.qty = 1
    msg.bs_flag = base_pb2.OPR_PURCHASE
    msg.market = 1
    msg.trader_id = trade_id
    msg.trader_ip = trade_ip
    msg.instrument_type = 0x40
    msg.basket_amount = msg.price * msg.qty
    msg.policy_id = policy_id
    success[robot.send(base_pb2.CMD_ETF_ORDER_REQ, msg, RespCallBack)] = ['CMD_ETF_ORDER_REQ 申购-上海', False]
    unitest_count += 1

    #   赎回-上海
    msg = stock_trade_pb2.SingleOrderReq()
    msg.code = '510030'
    msg.price = 21111
    msg.qty = 1
    msg.bs_flag = base_pb2.OPR_REDEEM
    msg.market = 1
    msg.trader_id = trade_id
    msg.trader_ip = trade_ip
    msg.instrument_type = 0x40
    msg.basket_amount = msg.price * msg.qty
    msg.policy_id = policy_id
    success[robot.send(base_pb2.CMD_ETF_ORDER_REQ, msg, RespCallBack)] = ['CMD_ETF_ORDER_REQ 赎回-上海', False]
    unitest_count += 1

    # CMD_FUND_ORDER_REQ
    #   快申
    msg = stock_trade_pb2.SingleOrderReq()
    msg.code = '399330'
    msg.price = 21111
    msg.qty = 1
    msg.bs_flag = base_pb2.OPR_FAST_PURCHASE
    msg.market = 0
    msg.trader_id = trade_id
    msg.trader_ip = trade_ip
    msg.instrument_type = 0x40
    msg.basket_amount = msg.price * msg.qty
    msg.policy_id = policy_id
    success[robot.send(base_pb2.CMD_FUND_ORDER_REQ, msg, RespCallBack)] = ['CMD_FUND_ORDER_REQ 快申', False]
    unitest_count += 1

    #   快赎
    msg = stock_trade_pb2.SingleOrderReq()
    msg.code = '399330'
    msg.price = 21111
    msg.qty = 1
    msg.bs_flag = base_pb2.OPR_FAST_REDEEM
    msg.market = 0
    msg.trader_id = trade_id
    msg.trader_ip = trade_ip
    msg.instrument_type = 0x40
    msg.basket_amount = msg.price * msg.qty
    msg.policy_id = policy_id
    success[robot.send(base_pb2.CMD_FUND_ORDER_REQ, msg, RespCallBack)] = ['CMD_FUND_ORDER_REQ 快赎', False]
    unitest_count += 1

    #   赎 + 卖
    msg = stock_trade_pb2.SingleOrderReq()
    msg.code = '399330'
    msg.price = 21111
    msg.qty = 1
    msg.bs_flag = base_pb2.OPR_REDEEM_SELL
    msg.market = 0
    msg.trader_id = trade_id
    msg.trader_ip = trade_ip
    msg.instrument_type = 0x40
    msg.basket_amount = msg.price * msg.qty
    msg.policy_id = policy_id
    success[robot.send(base_pb2.CMD_FUND_ORDER_REQ, msg, RespCallBack)] = ['CMD_FUND_ORDER_REQ 赎 + 卖', False]
    unitest_count += 1

    #   申购
    msg = stock_trade_pb2.SingleOrderReq()
    msg.code = '399330'
    msg.price = 21111
    msg.qty = 1
    msg.bs_flag = base_pb2.OPR_PURCHASE
    msg.market = 0
    msg.trader_id = trade_id
    msg.trader_ip = trade_ip
    msg.instrument_type = 0x40
    msg.basket_amount = msg.price * msg.qty
    msg.policy_id = policy_id
    success[robot.send(base_pb2.CMD_FUND_ORDER_REQ, msg, RespCallBack)] = ['CMD_FUND_ORDER_REQ 申购', False]
    unitest_count += 1

    #   赎回
    msg = stock_trade_pb2.SingleOrderReq()
    msg.code = '399330'
    msg.price = 21111
    msg.qty = 1
    msg.bs_flag = base_pb2.OPR_REDEEM
    msg.market = 0
    msg.trader_id = trade_id
    msg.trader_ip = trade_ip
    msg.instrument_type = 0x40
    msg.basket_amount = msg.price * msg.qty
    msg.policy_id = policy_id
    success[robot.send(base_pb2.CMD_FUND_ORDER_REQ, msg, RespCallBack)] = ['CMD_FUND_ORDER_REQ 赎回', False]
    unitest_count += 1

    #   买篮子
    msg = stock_trade_pb2.SingleOrderReq()
    msg.code = '399330'
    msg.price = 21111
    msg.qty = 1
    msg.bs_flag = base_pb2.OPR_BUY
    msg.market = 0
    msg.trader_id = trade_id
    msg.trader_ip = trade_ip
    msg.instrument_type = 0x40
    msg.basket_amount = msg.price * msg.qty
    msg.policy_id = policy_id
    success[robot.send(base_pb2.CMD_FUND_ORDER_REQ, msg, RespCallBack)] = ['CMD_FUND_ORDER_REQ 买篮子', False]
    unitest_count += 1

    #   卖篮子
    msg = stock_trade_pb2.SingleOrderReq()
    msg.code = '399330'
    msg.price = 21111
    msg.qty = 1
    msg.bs_flag = base_pb2.OPR_SELL
    msg.market = 0
    msg.trader_id = trade_id
    msg.trader_ip = trade_ip
    msg.instrument_type = 0x40
    msg.basket_amount = msg.price * msg.qty
    msg.policy_id = policy_id
    success[robot.send(base_pb2.CMD_FUND_ORDER_REQ, msg, RespCallBack)] = ['CMD_FUND_ORDER_REQ 卖篮子', False]
    unitest_count += 1

    #   买+申

    time.sleep(5)
    print u'总共测试数量\t', unitest_count, u'个。'
    clear_count = 0
    for i in success:
        if success[i][1]:
            clear_count += 1
        print '通过\t\t' if success[i][1] else "没有通过\t", success[i][0]
    print u'通过测试数量\t', clear_count, u'个。'
    print u'没通过测试数量\t', unitest_count-clear_count, u'个。'
    print u"*******[End]UniTest!*******"

    # pre_mkt = IL.DataSource.PreMktContainer.Memory()
    # print pre_mkt.get_structured_fund_codes('502010')
    # container = IL.DataSource.Container.Memory()
    # while True:
    #     time.sleep(1)
    #     print '*************订阅数据接口****************'
    #     print 'LimitUp', container.get_EtfInfo('510050', 'LimitUp')
    #     print 'LimitDow', container.get_EtfInfo('510050', 'LimitDow')
    #     print 'Stop', container.get_EtfInfo('510050', 'Stop')
    #     print 'MaxPrice', container.get_EtfInfo('510050', 'MaxPrice')
    #     print 'MinPrice', container.get_EtfInfo('510050', 'MinPrice')
    #     print 'DIOPVB1', container.get_EtfInfo('510050', 'DIOPVB1')
    #     print 'DIOPVS1', container.get_EtfInfo('510050', 'DIOPVS1')
    #     print 'Basis_B1', container.get_EtfInfo('510050', 'Basis_B1')
    #     print 'Basis_S1', container.get_EtfInfo('510050', 'Basis_S1')
    #     print 'Basis_Open', container.get_EtfInfo('510050', 'Basis_Open')
    #     print 'Basis_Close', container.get_EtfInfo('510050', 'Basis_Close')

def Main():
    config = ConfigFileData.Data()

    # 开启后台分发线程
    bg_thread = BasketThread.Distribute()
    bg_thread.setDaemon(True)
    bg_thread.start()

    # 开启Robot连接线程
    robot_thread = Robot.Connection()
    robot_thread.start()

    # 开启行情数据获取线程
    # f1_thread = RTS3_Stock()
    # f1_thread.start()
    #
    # f2_thread = RTS3_Future()
    # f2_thread.start()

    # 开启连接StockServer的线程
    ssc = StockServerClient.Connection()
    ssc.start()

    # 开启盘前数据获取线程
    # premkt = pre_mkt.Connection()
    # premkt.start()

    gevent.sleep(1)  # 启动线程

    # 进入事件循环
    while 1:
        gevent.sleep(0.1)

def Run():
    t = threading.Thread(target=Main)
    t.setDaemon(True)
    t.start()
    time.sleep(1)

def unitest(**kwargs):
    pass

if __name__ == u"__main__":
    Run()
    # Unitest()
    time.sleep(2)

    import IL.DataSource.PreMktContainer
    premkt = IL.DataSource.PreMktContainer.Memory()
    premkt.unitest_print()

    # import IL.Socket.StockServerClient
    # stock_server = IL.Socket.StockServerClient.Connection()
    #
    # msg = stock_trade_pb2.SingleOrderReq()
    # msg.code = '000062'
    # msg.price = 101000
    # msg.qty = 1000
    # msg.bs_flag = base_pb2.OPR_BUY
    # msg.market = 0
    # msg.trader_id = '192.168.1.116'
    # msg.trader_ip = '192.168.1.116'
    # msg.instrument_type = 0x10
    # msg.basket_amount = 0
    # msg.policy_id = ''
    # stock_server.send(base_pb2.CMD_SINGLE_ORDER_REQ, msg)

    import IL.Socket.Interface
    inf = IL.Socket.Interface.Trade()
    inf.set_trade_id_ip("192.168.1.116", "192.168.1.116")
    import IL.DataSource.Container
    container = IL.DataSource.Container.Memory()
    while 1:
        print """
1.下单
2.撤单
3.查询持仓
4.查询资金
5.查询成交
6.查询委托
        """
        select = raw_input('>')
        if select == '1':
            code = raw_input('\tcode:')
            price = raw_input('\tprice:')
            qty = raw_input('\tqty:')
            bs_flag = raw_input('\tbs_flag:')
            policyID = raw_input('\tpolicyID, 想成交填写trade:')
            inf.SingleOrder(unitest, code, int(float(price)*10000), int(qty), int(bs_flag), policyID)
        elif select == '2':
            order_no = raw_input('\torder_no:')
            market = raw_input('\tmarket:')
            inf.SingleCancel(unitest, order_no, int(market))
        elif select == '3':
            posit_code = raw_input("请输入需要查询的股票代码，全部输入all:")
            if posit_code == 'all':
                print inf.get_Position(SearchCodeList=[posit_code])
            else:
                print inf.get_Position()
        elif select == '4':
            print inf.get_Money()
        elif select == '5':
            input = raw_input("请输入委托编号：")
            print container.get_Trade_order_no(input)

        elif select == '6':
            input = raw_input("请输入委托编号：")
            if input == 'all':
                print inf.get_OrderList()
            else:
                print container.get_Order_order_no(input)

        time.sleep(0.5)
        print '****************执行完毕***************'
        print


    time.sleep(500)



